import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Template } from 'meteor/templating';

import * as JoinLinks from '/imports/api/joinLinks/methods';

import { ButtonCancel } from '/imports/ui/components/buttons';

import './styles.scss';

export function JoinLinkRebuild(props: { tenantId: string }) {
	const { tenantId } = props;

	const { t } = useTranslation();
	const [confirm, setConfirm] = useState(false);

	if (!confirm) {
		return (
			<button
				className="btn btn-add"
				type="button"
				onClick={() => {
					setConfirm(true);
				}}
			>
				{t('joinLink.rebuild', 'Create a new link')}
			</button>
		);
	}

	return (
		<div className="join-link-rebuild-confirmation">
			<p>
				{t(
					'joinLink.reallyrebuild',
					'Please confirm that you would like to create a new link. Existing link will be revoked.',
				)}
			</p>
			<div className="form-actions">
				<button
					className="btn btn-add"
					type="button"
					onClick={async () => {
						await JoinLinks.create(tenantId);
						setConfirm(false);
					}}
				>
					{t('joinLink.rebuild', 'Create a new link')}
				</button>
				<ButtonCancel
					onClick={() => {
						setConfirm(false);
					}}
				/>
			</div>
		</div>
	);
}

// for Blaze
// eslint-disable-next-line import/first
import './template.html';

Template.registerHelper('JoinLinkRebuild', () => JoinLinkRebuild);
